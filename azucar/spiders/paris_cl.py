# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule, Spider
from azucar.items import AzucarLoader
from scrapy.http import Request,FormRequest
import time

class paris_cl(CrawlSpider):
	name = 'paris_cl'
	allowed_domains = ['paris.cl']
	start_urls = [#"https://www.paris.cl/webapp/wcs/stores/servlet/SearchDisplay?searchTermScope=&searchType=1000&filterTerm=&orderBy=2&maxPrice=&showResultsPage=true&langId=-5&beginIndex=0&sType=SimpleSearch&metaData=&manufacturer=&resultCatEntryType=&catalogId=40000000629&pageView=image&searchTerm=&minPrice=&categoryId=51206300&storeId=10801&pageSize=100"]
				 "https://www.paris.cl/webapp/wcs/stores/servlet/SearchDisplay?searchType=1000&showResultsPage=true&langId=-5&beginIndex=0&sType=SimpleSearch&catalogId=40000000629&pageView=image&categoryId=51206300&storeId=10801&pageSize=100"]
	#rules = (
	#   	 Rule(LinkExtractor(allow=('es\/paris\/(decohogar)\/.*\d+-ppp-')), follow=True,callback='parse_items'),

	#)
	def parse(self,response):
		yield FormRequest(url="https://www.paris.cl/webapp/wcs/stores/servlet/AjaxCatalogSearchResultView?searchTermScope=&searchType=1000&filterTerm=&orderBy=2&maxPrice=&showResultsPage=true&langId=-5&beginIndex=30&sType=SimpleSearch&metaData=&pageSize=30&manufacturer=&resultCatEntryType=&catalogId=40000000629&pageView=image&searchTerm=&minPrice=&pageNumber=2&categoryId=51206300&storeId=10801"
						, formdata={'searchResultsPageNum':'30'
									,'searchResultsView': ''
									,'searchResultsURL':'https://www.paris.cl/webapp/wcs/stores/servlet/AjaxCatalogSearchResultView?searchTermScope=&searchType=1000&filterTerm=&orderBy=2&maxPrice=&showResultsPage=true&langId=-5&beginIndex=30&sType=SimpleSearch&metaData=&pageSize=30&manufacturer=&resultCatEntryType=&catalogId=40000000629&pageView=image&searchTerm=&minPrice=&pageNumber=2&categoryId=51206300&storeId=10801'
									,'objectId':''
									,'requesttype':'ajax'
								   }
						, callback = self.prueba)



	def prueba (self,response):
		print(response.xpath('//td[@class="item"]//@href').extract())


	def parse_items(self, response):
		loader = AzucarLoader(response=response)
		loader.add_xpath( 'nombre', "//div[@id='titulo-transmisor']/h1[@id='catalog_link']/text()")
		loader.add_xpath( 'id_producto', "//div[@class='description']/p[1]/text()")
		#loader.add_xpath( 'marca', "//div[@class='right']/div[@class='description']//text()")
		loader.add_xpath( 'categoria', "//div[@id='breadcrumb']/div[@id='WC_BreadCrumbTrailDisplay_div_1']//text()")

		loader.add_xpath( 'detalles_generales', "//div[@class='description']/p[3]//text()")
		loader.add_xpath( 'detalles_precios', "//div[@class='item-price-details']//text()")
		#loader.add_xpath( 'disponibilidad',"//div[@class='right']/div[@class='description']//text()")
		loader.add_xpath( 'url_imagen','//*[@id="zoom1"]//@href')

		loader.add_value( 'url', response.url)
		loader.add_value( 'fuente','www.paris.cl')
		loader.add_value( 'fecha_extraccion', time.strftime("%d/%m/%Y"))
		loader.add_value( 'hora_extraccion', time.strftime("%H:%M:%S"))

		yield (loader.load_item())
