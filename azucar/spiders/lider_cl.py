
# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Request
from azucar.items import AzucarLoader
import time


class lider_cl(CrawlSpider):
    name = 'lider_cl'
    allowed_domains = ['lider.cl']
    start_urls = [ "https://www.lider.cl/supermercado/category/Congelados-Postres/Helados/_/N-ovueji?N=&No=0&Nrpp=200",
                   "https://www.lider.cl/supermercado/category/Congelados-Postres/_/N-1jtxo1l?N=&No=0&Nrpp=600",
                   "https://www.lider.cl/supermercado/category/Desayuno-Dulces/Confiter%C3%ADa/_/N-1juh1iq?N=&No=0&Nrpp=700",
                   "https://www.lider.cl/supermercado/category/Desayuno-Dulces/Reposter%C3%ADa/_/N-19f01mj?N=&No=0&Nrpp=200",
                   "https://www.lider.cl/supermercado/category/Desayuno-Dulces/Postres-para-Preparar/_/N-6vmfx7?N=&No=0&Nrpp=100",
                   "https://www.lider.cl/supermercado/category/Desayuno-Dulces/Az%C3%BAcar-y-Endulzantes/_/N-1j5bt7c?N=&No=0&Nrpp=200",
                   "https://www.lider.cl/supermercado/category/Desayuno-Dulces/Dulces-y-Mermeladas/_/N-pbmgle?N=&No=0&Nrpp=200",
                   "https://www.lider.cl/supermercado/category/Desayuno-Dulces/Caf%C3%A9-T%C3%A9-y-Hierbas/_/N-wauza0?N=&No=0&Nrpp=400",
                   "https://www.lider.cl/supermercado/category/Desayuno-Dulces/Cereales/Cereales/_/N-e00l8z?N=&No=0&Nrpp=200"]
    rules = (
            Rule(LinkExtractor(allow=('product\/.*\/\d+$')), follow=True,callback='parse_items'),

    )

    def parse_items(self, response):
        loader = AzucarLoader(response=response)
        loader.add_xpath( 'nombre', "//span[@id='span-display-name']/text()")
        loader.add_xpath( 'id_producto', "//div[@id='wrap-btn-agregar-pdp']/button[@class='btn btn-info btn-block btn-agregar js-btn-agregar-pdp']/@sku-id")
        loader.add_xpath( 'marca', "//div[@class='product-info col-lg-3 col-md-5 col-sm-5 col-xs-10']/h1/span[1]/text()")
        loader.add_xpath( 'peso',"//div[@id='loadPDPCon']/div[@class='product-info col-lg-3 col-md-5 col-sm-5 col-xs-10']/h1/span[3]/text()")
        loader.add_xpath( 'categoria', "//section[@id='content']/main[@class='content']/div[@class='container-fluid box-breadcrumb']/ol[@class='breadcrumb col-md-12 hidden-xs']//text()")

        loader.add_xpath( 'detalles_generales', "//div[@class='feature']//text()")
        loader.add_xpath( 'detalles_precios', "//div[@id='productPrice']//text()") 
      
        loader.add_value( 'url', response.url)
        loader.add_value( 'fuente','www.lider.cl')
        loader.add_value( 'fecha_extraccion', time.strftime("%d/%m/%Y"))
        loader.add_value( 'hora_extraccion', time.strftime("%H:%M:%S"))

        yield (loader.load_item())

