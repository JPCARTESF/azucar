
# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Request
from azucar.items import AzucarlafeteLoader
import time


class lafete_com(CrawlSpider):
    name = 'lafete_com'
    allowed_domains = ['lafetechocolat.com']
    start_urls = ["http://www.lafetechocolat.com"]

    def parse(self,response):
        for link in response.xpath("//div[@class='row']/div[@class='col-md-3 col-sm-12 col-xs-12 single-category CCatPlus list-group-item-heading']/div[@class='row']/a//@href").extract():
            yield Request( url=link,callback=self.parse_data)

    def parse_data(self, response):
        for row  in response.xpath("//div[@class='product-container ']"):
            loader = AzucarlafeteLoader(response=response, selector = row)
            loader.add_xpath( 'nombre', ".//h5/a[@class='product-name']//text()")
            loader.add_xpath( 'id_producto', ".//a/@data-id-product")
            loader.add_value( 'marca', "lafete")
            loader.add_xpath( 'peso',".//span[@class='weightProduct text-center']//text()")
            ##loader.add_xpath( 'categoria', "//section[@id='content']/main[@class='content']/div[@class='container-fluid box-breadcrumb']/ol[@class='breadcrumb col-md-12 hidden-xs']//text()")

            loader.add_xpath( 'url_imagen',".//img[@class='replace-2x img-responsive']/@src")
           # loader.add_xpath( 'detalles_generales', "//div[@class='description']//text()")
            precio = row.xpath(".//div[@class='content_price']//span[@class='price product-price']//text()").extract_first()
            loader.add_value( 'detalles_precios', precio) 
          
            loader.add_xpath( 'url',".//a[@class='lnk_view visible-md']//@href" )
            loader.add_value( 'fuente','www.lafetechocolat.cl')
            loader.add_value( 'fecha_extraccion', time.strftime("%d/%m/%Y"))
            loader.add_value( 'hora_extraccion', time.strftime("%H:%M:%S"))

            yield (loader.load_item())


