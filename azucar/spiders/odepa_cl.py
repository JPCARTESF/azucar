# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Request
from azucar.items import AzucarOdepaLoader
from datetime import datetime, date, time, timedelta
import time


class odepa_cl(CrawlSpider):
    name = 'odepa_cl'
    allowed_domains = ['odepa.cl']
    start_urls = ["http://apps.odepa.cl/menu/PreciosConsumidorNS.action?consumidor=frutas&reporte=precios_consumidor"]


    def parse(self,response):
        #Se obtiene la ultima semana que se registra en odepa
        fechas_semana = response.xpath("//*[@class='texto_parametros']//text()").extract_first().replace(' ','')

        fecha_fin    = datetime.strptime(fechas_semana.split('-')[1], "%d/%m/%Y").date()
        fecha_inicio = fecha_fin - timedelta(days=5)

        anoSem =   fecha_inicio.strftime('%Y')
        #Se establece el primer dia del mes y se le resta un día para obtener el mes anterior del año.
        #glosa_ipc  = (fecha_inicio.replace(day=1) - timedelta(days=1)).strftime('%m/%Y') 
        glosa_ipc = response.xpath("//*[@id='codIpc']//option[position()=1]//text()").extract_first().replace(" ",'')
        mesFin =     fecha_fin.strftime('%m')
        anoMesFin =  fecha_fin.strftime('%Y')
        anoFin =     fecha_fin.strftime('%Y')
        semanaFin =  fecha_fin.strftime('%W')

        semanaIni =  fecha_inicio.strftime('%W')
        mesIni =     fecha_inicio.strftime('%m')
        anoMesIni =  fecha_inicio.strftime('%Y')
        anoIni =     fecha_inicio.strftime('%Y')
        regiones = []
        regiones.append(('Maule','7'))
        regiones.append(('Coquimbo','4'))
        regiones.append(('Valparaíso','5'))
        regiones.append(('Metropolitana','13'))
        regiones.append(('Bíobío','8'))
        regiones.append(('La Araucanía','9'))
        regiones.append(('Los Lagos','10'))
        for row in regiones:
            yield Request(url="http://apps.odepa.cl/precios/PreciosConsumidorResultNS.action;jsessionid=49C2574F645AC2090975B8807649581E?returnURL=precios_consumidorNS&params.glosaSector=Todos+(detallado+por+sector)&params.glosaTipoProducto=Az%C3%BAcar&params.glosaProducto=Az%C3%BAcar&params.glosaTPM=Supermercado&params.glosaIpc="+glosa_ipc+"&params.glosaRegion="+row[0]+"&params.tipoSerie=1&params.anoSem="+anoSem+"&params.semanaIni="+semanaIni+"&params.semanaFin="+semanaFin+"&params.mesIni="+mesIni+"&params.anoMesIni="+anoMesIni+"&params.mesFin="+mesFin+"&params.anoMesFin="+anoMesFin+"&params.anoIni="+anoIni+"&params.anoFin="+anoFin+"&params.codRegion="+row[1]+"&params.codSector=100&params.codTipoProducto=9&params.codTipificacion=8&__checkbox_params.codTipificacion=8&params.codTipificacion=9&__checkbox_params.codTipificacion=9&params.codProducto=351&__multiselect_params.codProducto=&params.codTipoPuntoMonitoreo=1&__multiselect_params.codTipoPuntoMonitoreo=&params.codTipoPrecio=2"
                        , callback=self.parse_data)
        #FALTA ARICA 
        yield Request(url="http://apps.odepa.cl/precios/PreciosConsumidorResultNS.action;jsessionid=80816E365D8899E4F349187846DD3E51?returnURL=precios_consumidorNS&params.glosaSector=Arica&params.glosaTipoProducto=Az%C3%BAcar&params.glosaProducto=Az%C3%BAcar&params.glosaTPM=Supermercado&params.glosaIpc="+glosa_ipc+"&params.glosaRegion=Arica y Parinacota&params.tipoSerie=1&params.anoSem="+anoSem+"&params.semanaIni="+semanaIni+"&params.semanaFin="+semanaFin+"&params.mesIni="+mesIni+"&params.anoMesIni="+anoMesIni+"&params.mesFin="+mesFin+"&params.anoMesFin="+anoMesFin+"&params.anoIni="+anoIni+"&params.anoFin="+anoFin+"&params.codRegion=15&params.codSector=30&params.codTipoProducto=9&params.codTipificacion=8&__checkbox_params.codTipificacion=8&params.codTipificacion=9&__checkbox_params.codTipificacion=9&params.codProducto=351&__multiselect_params.codProducto=&params.codTipoPuntoMonitoreo=1&__multiselect_params.codTipoPuntoMonitoreo=&params.codTipoPrecio=2"
                     ,callback= self.parse_data_arica)  

    def parse_data(self, response):
        for row in response.xpath("//table[@id='tbl']//tr[position()<last() and position()>1]"):
            loader = AzucarOdepaLoader(response= response,selector=row)
            loader.add_xpath( 'nro_semana'     , './td[1]/text()')
            loader.add_xpath( 'fecha_inicio'   , './td[2]/text()')
            loader.add_xpath( 'fecha_fin'      , './td[3]/text()')
            loader.add_xpath( 'sector'         , './td[4]/text()')
            loader.add_value( 'monitoreo'      ,  'Supermercado')
            loader.add_value( 'producto'       ,  'Azucar')
#           loader.add_xpath( 'monitoreo'      , './td[5]/text()')
#           loader.add_xpath( 'producto'       , './td[6]/text()')
            loader.add_xpath( 'unidad'         , './td[5]/text()')
            #loader.add_xpath( 'procedencia'    , './td[8]/text()')
            loader.add_xpath( 'precio_minimo'  , './td[6]/text()')
            loader.add_xpath( 'precio_maximo'  , './td[7]/text()')
            loader.add_xpath( 'precio_promedio', './td[8]/text()')
            loader.add_value( 'url', response.url)
            loader.add_value( 'fuente','www.odepa.cl')
            loader.add_value( 'fecha_extraccion',time.strftime("%d/%m/%y"))
            loader.add_value( 'hora_extraccion', time.strftime("%H:%M:%S"))
            yield (loader.load_item())
    def parse_data_arica(self, response):
        for row in response.xpath("//table[@id='tbl']//tr[position()<last() and position()>1]"):
            loader = AzucarOdepaLoader(response= response,selector=row)
            loader.add_xpath( 'nro_semana'     , './td[1]/text()')
            loader.add_xpath( 'fecha_inicio'   , './td[2]/text()')
            loader.add_xpath( 'fecha_fin'      , './td[3]/text()')
            loader.add_value( 'sector'         , '  Arica')
            loader.add_value( 'monitoreo'      ,  'Supermercado')
            loader.add_value( 'producto'       ,  'Azucar')
#           loader.add_xpath( 'monitoreo'      , './td[5]/text()')
#           loader.add_xpath( 'producto'       , './td[6]/text()')
            loader.add_xpath( 'unidad'         , './td[4]/text()')
            #loader.add_xpath( 'procedencia'    , './td[8]/text()')
            loader.add_xpath( 'precio_minimo'  , './td[5]/text()')
            loader.add_xpath( 'precio_maximo'  , './td[6]/text()')
            loader.add_xpath( 'precio_promedio', './td[7]/text()')
            loader.add_value( 'url', response.url)
            loader.add_value( 'fuente','www.odepa.cl')
            loader.add_value( 'fecha_extraccion',time.strftime("%d/%m/%y"))
            loader.add_value( 'hora_extraccion', time.strftime("%H:%M:%S"))
            yield (loader.load_item())

