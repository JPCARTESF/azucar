
# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Request
from azucar.items import AzucarLoader
import time


class varsovienne_cl(CrawlSpider):
    name = 'varsovienne_cl'
    allowed_domains = ['varsovienne.cl']
    start_urls = ["http://www.varsovienne.cl/bombones",
                  "http://www.varsovienne.cl/chocolate",
                  "http://www.varsovienne.cl/alfajores-galletas",
                  "http://www.varsovienne.cl/calugas",
                  "http://www.varsovienne.cl/tentaciones",
                  "http://www.varsovienne.cl/gustos-del-mundo",
                  "http://www.varsovienne.cl/sin-azucar"]


    rules = (
            Rule(LinkExtractor(allow=('p=\d+'))),
            Rule(LinkExtractor(allow=('\.html$')), follow=True,callback='parse_data'),

    )
    def parse_data(self, response):
        loader = AzucarLoader(response=response)
        loader.add_xpath( 'nombre', "//div[@class='product-name']/h1/text()")
        loader.add_xpath( 'id_producto', '//*[@id="product_addtocart_form"]/div[2]/div[2]/meta/@content')
        #loader.add_xpath( 'marca', "lafete")
        peso = response.xpath("//div[@class='product-name']/h1/text()").extract_first()
        if peso.find(',')!=-1:
          peso = peso.split(',')[1].replace(".","")
        loader.add_value( 'peso',peso)
        loader.add_xpath( 'categoria', "//div[@class='breadcrumbs']/ul//text()")

        loader.add_xpath( 'url_imagen','//*[@id="image-main"]//@src')
        loader.add_xpath( 'detalles_generales', "//div[@class='short-description']/div[@class='std']//text()")
        loader.add_xpath( 'detalles_precios', "//span[@class='price']//text()") 
      
        loader.add_value( 'url', response.url)
        loader.add_value( 'fuente','www.varsovienne.cl')
        loader.add_value( 'fecha_extraccion', time.strftime("%d/%m/%Y"))
        loader.add_value( 'hora_extraccion', time.strftime("%H:%M:%S"))

        yield (loader.load_item())

