
# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Request
from azucar.items import AzucarLoader
import time




class tottus_cl(CrawlSpider):
    name = 'tottus_cl'
    allowed_domains = ['tottus.cl']
    start_urls = [#"http://www.tottus.cl/tottus/browse/Desayuno-y-Dulces-Cereales/_/N-1doux0u?Nrpp=2000",
                  #"http://www.tottus.cl/tottus/browse/Desayuno-y-Dulces-Az%C3%BAcar-y-Endulzante/_/N-1l6nuez?Nrpp=2000",
                  #"http://www.tottus.cl/tottus/browse/Desayuno-y-Dulces-Mermeladas-y-Dulces/_/N-kk0qzt?Nrpp=2000",
                  #"http://www.tottus.cl/tottus/browse/Desayuno-y-Dulces-Caramelos-y-Chocolates/_/N-18stt2d?Nrpp=2000",
                  #"http://www.tottus.cl/tottus/browse/Desayuno-y-Dulces-Colaciones-y-Galletas/_/N-1fbr8a3?Nrpp=2000",
                  #"http://www.tottus.cl/tottus/browse/Desayuno-y-Dulces-Cereales/_/N-1doux0u?Nrpp=2000"
                  "http://www.tottus.cl/tottus/browse/Supermercado-Desayuno-y-Dulces/_/N-1dl1jgf?Nrpp=20000"
                  ]


    def parse(self,response):
        for link in response.xpath('//div[@id="category-products-items"]//a/@href').extract():
            yield Request(
                url='http://www.tottus.cl'+link,
                callback=self.parse_items, #Callback
                 )

    
    def parse_items(self, response):
        loader = AzucarLoader(response= response)
        #loader.add_xpath( 'id_producto','//*[@id="product-main"]//@data-bootic-productid')
        loader.add_xpath( 'id_producto','//div[@class="price-selector"]//input[@class="atg_behavior_addItemToCart btn-add-cart"]/@value')
        loader.add_xpath( 'categoria',"//div[@class='row']/div[@class='col-md-12 col-sm-12']/div[@class='title-category-section breadcrumb-nav']//text()")
        loader.add_xpath( 'nombre', "//div[@class='title']/h5//text()[1]")
        loader.add_xpath( 'detalles_precios',"//div[@class='price-selector']/div[@class='prices']//text()")
        #loader.add_xpath( 'disponible', "//form[@class='add_to_cart form-add-product-132756']/p[@class='bootic_alert out_of_stock']//text()") #si esta disponible se agrega al carrito y si no manda un mensaje de alerta y no se puede agregar al carrito
        loader.add_xpath( 'url_imagen' ,"//div[@class='col-lg-4 col-md-5 col-sm-5 caption-img']//@src")
        loader.add_xpath( 'peso',"//div[@class='row']/div/div[@class='statement']/text()")
        loader.add_xpath( 'marca',"//div[@class='row']/div//div[@class='title']/h5/span/text()")
        if not response.xpath("//div[@class='col-lg-4 col-md-5 col-sm-5 caption-img']//@src").extract():
            loader.add_value( 'url_imagen', 'NA')

        loader.add_xpath( 'detalles_precios',"//div[@class='price-selector']//text()")
        loader.add_value( 'url', response.url)
        loader.add_value( 'fuente','www.tottus.cl')
        loader.add_value( 'fecha_extraccion', time.strftime("%d/%m/%Y"))
        loader.add_value( 'hora_extraccion', time.strftime("%H:%M:%S"))

        yield (loader.load_item())



