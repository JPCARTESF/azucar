# -*- coding: utf-8 -*-
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Request
from azucar.items import JumboLoader
import time
from w3lib.url import url_query_parameter



class jumbo_cl(CrawlSpider):
    name = 'jumbo_cl'
    #allowed_domains = ['telemercado.cl']
    start_urls = ["http://www.jumbo.cl/FO/CategoryDisplay?cab=4007&int=3782&ter=-1"]
   
    urls = ["http://www.jumbo.cl/FO/PasoDosResultado?cab=4009&int=3802&ter=-1"  #Azucar y endulzantes
           ,"http://www.jumbo.cl/FO/PasoDosResultado?cab=4009&int=3086&ter=-1"  #Colaciones
           ,"http://www.jumbo.cl/FO/PasoDosResultado?cab=4009&int=12&ter=-1"    #Galletas y golosinas
           ,"http://www.jumbo.cl/FO/PasoDosResultado?cab=4009&int=3803&ter=-1"  #Mermeladas y manjares
           ,"http://www.jumbo.cl/FO/PasoDosResultado?cab=4009&int=3849&ter=-1"  #Postres
           ,"http://www.jumbo.cl/FO/PasoDosResultado?cab=4011&int=3783&ter=-1"  #Helados
    ]


    def parse(self,response):
        for link in self.urls:
            yield Request(url=link
                             ,dont_filter=True
                             ,callback=self.parse_data)

    def define_categoria (self,value):
        par = url_query_parameter(value, 'int')
        if value.find('3802')!=-1:
            return "Azucar y Endulzantes"
        elif value.find('3086')!=-1:
            return "Colaciones"
        elif value.find('12')!=-1:
            return "Galletas y Golosinas"
        elif value.find('3803')!=-1:
            return "Mermeladas y Manjar"
        elif value.find('3849')!=-1:
            return "Postres"
        elif value.find('3783')!=-1:
            return "Helados"
        else:
            return "NA"

    def parse_data(self,response):
        for row in response.xpath("//ul[@class='grid']/li"):
            loader = JumboLoader(response=response, selector = row)
            url = row.xpath(".//img[@id='img_{pro_id}']/@src").extract_first()
           # marca = row.xpath(".//span[@class='Marca']//text()").extract_first()
            marca = row.xpath(".//*[@class='Marca' or @class='txt_marca_h']//text()").extract_first()
            detalles_precios = row.xpath("concat(.//*[@class='txt_precio_h'],' & ',.//*[@class='txt_precio_medida_h'])").extract_first()
            precio_antes = marca

            if not marca:
                marca = 'SIN MARCA'

            if str(precio_antes).find('ANTES')!=-1:
                marca = 'SIN MARCA'
                detalles_precios = detalles_precios + '&' + precio_antes


            
            loader.add_xpath( 'producto'            , ".//div[@class='txt_nombre_h']/a[@id='ficha']//text()")
            loader.add_xpath( 'id_producto'         , ".//div[@class='txt_nombre_h']/a[@id='ficha']/@href")
            loader.add_xpath( 'precio_medida'       , ".//div[@class='txt_precio_medida_h']//text()")
            loader.add_xpath( 'precio'              , ".//div[@class='txt_precio_h']//text()")        
            loader.add_value( 'detalles_precios'    , detalles_precios ) 

            loader.add_value( 'categoria'           , self.define_categoria(response.url))
            loader.add_value( 'marca'               ,  marca )
            loader.add_value( 'url_imagen'          , 'https://www.jumbo.cl' + url )
            loader.add_value( 'url'                 ,  response.url)
            loader.add_value( 'fuente'              , 'www.jumbo.cl')
            loader.add_value( 'fecha_extraccion'    , time.strftime("%d/%m/%Y"))
            loader.add_value( 'hora_extraccion'     , time.strftime("%H:%M:%S"))

            yield (loader.load_item())
