# -*- coding: utf-8 -*-
from azucar import items
from azucar import settings
from azucar.items import *
import csv



class AzucarPipeline(object):
	def __init__(self):
		self.file_name = {} 
		self.keys = {'id_producto','nombre','categoria','marca','peso','url_imagen','hora_extraccion','fecha_extraccion','fuente','url','url_imagen','disponibilidad','detalles_precios','detalles_generales'}
		self.keys_jumbo = {'producto','categoria','id_producto','precio_medida','precio','detalles_precios','marca','url_imagen','url','fuente','fecha_extraccion','hora_extraccion'}

	def open_spider(self, spider):
		self.file_name=csv.writer(open('output_'+spider.name+'.csv','w'))
		if spider.name.find('jumbo_cl')!=-1:
			self.file_name.writerow(['producto','categoria','id_producto','precio_medida','precio','detalles_precios','marca','url_imagen','url','fuente','fecha_extraccion','hora_extraccion'])

		elif spider.name.find('odepa')!=-1:
			self.file_name.writerow(['nro_semana','fecha_inicio','fecha_fin','sector','monitoreo','producto','unidad','precio_minimo','precio_maximo','precio_promedio','fuente','fecha_extraccion','hora_extraccion','url'])
		else:
			self.file_name.writerow(['id_producto','nombre','categoria','marca','peso','hora_extraccion','fecha_extraccion','fuente','url','url_imagen','disponibilidad','detalles_precios','detalles_generales'])

	def process_item(self, item, spider):
		if spider.name.find('jumbo_cl')!=-1:  
			for key in self.keys_jumbo:
				item.setdefault(key,'NA')      
			self.file_name.writerow([
									item['producto'],
									item['categoria'],
									item['id_producto'],
									item['precio_medida'],
									item['precio'],
									item['detalles_precios'],
									item['marca'],
									item['url_imagen'],
									item['url'],
									item['fuente'],
									item['fecha_extraccion'],
									item['hora_extraccion'],
							])

			return item
		elif spider.name.find('odepa')!=-1:
			self.file_name.writerow([   item['nro_semana'],
										item['fecha_inicio'],
										item['fecha_fin'],
										item['sector'],
										item['monitoreo'],
										item['producto'],
										item['unidad'],
										item['precio_minimo'],
										item['precio_maximo'],
										item['precio_promedio'],
										item['fuente'],
										item['fecha_extraccion'],
										item['hora_extraccion'],
										item['url']
								])
			return item
		else: 

			for key in self.keys:
				item.setdefault(key,'NA')

			self.file_name.writerow([
										item['id_producto'],
										item['nombre'],
										item['categoria'],
										item['marca'],
										item['peso'],
										item['hora_extraccion'],
										item['fecha_extraccion'],
										item['fuente'],
										item['url'],
										item['url_imagen'],
										item['disponibilidad'],
										item['detalles_precios'],
										item['detalles_generales'],
										])
			return item



