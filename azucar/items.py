# -*- coding: utf-8 -*-

import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity, Compose, Join, MapCompose, TakeFirst
from w3lib.html import remove_tags
import time


class AzucarItem(scrapy.Item):
    id_producto = scrapy.Field()
    nombre  = scrapy.Field()
    categoria  = scrapy.Field()
    peso = scrapy.Field()
    marca = scrapy.Field()
    url_imagen = scrapy.Field()
    hora_extraccion = scrapy.Field()
    fecha_extraccion = scrapy.Field()
    fuente = scrapy.Field()
    url  = scrapy.Field()
    disponibilidad = scrapy.Field()
    #Datos no estructurados
    detalles_precios = scrapy.Field()
    detalles_generales = scrapy.Field()

    #SE AÑADEN COLUMNAS DE ODEPA
    nro_semana = scrapy.Field() 
    fecha_inicio = scrapy.Field()
    fecha_fin = scrapy.Field()
    sector = scrapy.Field()
    monitoreo = scrapy.Field()
    producto = scrapy.Field()
    unidad  = scrapy.Field() 
    procedencia = scrapy.Field() 
    precio_minimo = scrapy.Field() 
    precio_maximo = scrapy.Field()
    precio_promedio = scrapy.Field() 
               

 #Jumbo

    #producto = scrapy.Field()
    #id_producto = scrapy.Field()
    precio_medida = scrapy.Field()
    precio = scrapy.Field()
    #detalles_precios = scrapy.Field()
    #marca = scrapy.Field()
    #url_imagen = scrapy.Field()
    #url = scrapy.Field()
    #fuente = scrapy.Field()
    #fecha_extraccion = scrapy.Field()
    #hora_extraccion = scrapy.Field()
             

class JumboLoader(ItemLoader):
    default_item_class = AzucarItem

    def strip_dashes(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace(' ','').replace('*','').replace('\xa0','').replace('.','').strip()
    def strip_prices(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace("$",'').replace(".",'').replace('& &','&').replace(' ','')

    def strip_id_producto(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace("javascript:",'').replace(".",'').replace(',','').replace(';','').replace(' ','').replace(')','').replace('(','')
    
    #Datos no estructurados
    detalles_precios_in = Compose(Join(),strip_prices)
    precio_medida_in = Compose(Join(),strip_prices)
    precio_in = Compose(Join(),strip_prices)
    id_producto_in = Compose(Join(),strip_id_producto)


    producto_out =TakeFirst()
    marca_out = TakeFirst()
    precio_out = TakeFirst()
    #Valores de salida
    precio_medida_out = TakeFirst()
    id_producto_out = TakeFirst()
    url_imagen_out = TakeFirst()
    hora_extraccion_out = TakeFirst()
    fecha_extraccion_out = TakeFirst()
    fuente_out = TakeFirst()
    url_out  = TakeFirst()
    categoria_out = TakeFirst()
    detalles_precios_out = TakeFirst()
    id_producto_out = TakeFirst()  




class AzucarOdepaLoader(ItemLoader):
    default_item_class = AzucarItem

    def strip_dashes(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace('\xa0','')
   
    default_entrada =  Compose(Join(),strip_dashes) 
    default_salida = TakeFirst()

    nro_semana_in = default_entrada 
    fecha_inicio_in = default_entrada 
    fecha_fin_in = default_entrada 
    sector_in = default_entrada 
    monitoreo_in = default_entrada 
    producto_in = default_entrada 
    unidad_in = default_entrada 
    procedencia_in = default_entrada 
    precio_minimo_in = default_entrada 
    precio_maximo_in = default_entrada 
    precio_promedio_in = default_entrada 

    #Datos de salida
    nro_semana_out = default_salida 
    fecha_inicio_out = default_salida 
    fecha_fin_out = default_salida 
    sector_out = default_salida 
    monitoreo_out = default_salida 
    producto_out = default_salida 
    unidad_out = default_salida 
    procedencia_out = default_salida 
    precio_minimo_out = default_salida 
    precio_maximo_out = default_salida 
    precio_promedio_out = default_salida  
    hora_extraccion_out = default_salida
    fecha_extraccion_out = default_salida
    fuente_out = default_salida
    url_out    = default_salida





class AzucarLoader(ItemLoader):
    default_item_class = AzucarItem

    def price_processor(v):
        if v:
            return v.replace('.', '').replace('$','').replace('*','')
    def strip_dashes(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace(' ','').replace('*','').replace("\xa0","").strip()
    def strip_others(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','')


    categoria_in  = MapCompose(Join(),strip_dashes)
    peso_in = MapCompose(Join(),strip_dashes)
    #Datos no estructurados
    detalles_precios_in = MapCompose(Join(),strip_dashes)
    detalles_tecnicos_in = MapCompose(Join(),strip_dashes)
    #nombre_in = MapCompose(Join(),strip_dashes)
    #id_producto_in = MapCompose(Join(),strip_dashes)

    #Valores de salida
    url_imagen_out = TakeFirst()
    hora_extraccion_out = TakeFirst()
    fecha_extraccion_out = TakeFirst()
    fuente_out = TakeFirst()
    url_out  = TakeFirst()
    nombre_out = TakeFirst()
    id_producto_out = TakeFirst()


class AzucarlafeteLoader(ItemLoader):
    default_item_class = AzucarItem

    def price_processor(v):
        if v:
            return v.replace('.', '').replace('$','').replace('*','')
    def strip_dashes(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','').replace('*','').replace("\xa0","").strip()
    def strip_others(value):
        if value:
            return value.replace('\t','').replace('\n','').replace('\r','')


    categoria_in  = Compose(Join(),strip_dashes)
    peso_in = Compose(Join(),strip_dashes)
    #Datos no estructurados
    detalles_precios_in = Compose(Join(),strip_dashes)
    detalles_tecnicos_in = Compose(Join(),strip_dashes)
    nombre_in = Compose(Join(),strip_dashes)
    #id_producto_in = MapCompose(Join(),strip_dashes)

    #Valores de salida
    url_imagen_out = TakeFirst()
    categoria_out = TakeFirst()
    hora_extraccion_out = TakeFirst()
    fecha_extraccion_out = TakeFirst()
    fuente_out = TakeFirst()
    url_out  = TakeFirst()
    nombre_out = TakeFirst()
    id_producto_out = TakeFirst()
    marca_out = TakeFirst()
    detalles_precios_out = TakeFirst()
    peso_out= TakeFirst()

